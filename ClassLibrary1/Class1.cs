﻿using System;

namespace ClassLibrary1
{
    /// <summary>
    /// Konstruktor für Class1
    /// </summary>
    public class Class1
    {
        int i = 1;

        /// <summary>
        /// reader für die Klasse
        /// </summary>
        /// <param name="a">erster parameter</param>
        /// <param name="b">zweiter parameter</param>
        /// <returns>returnwert</returns>
        public string reader(string a, string b)
        {
            return "";
        }
    }    
}
