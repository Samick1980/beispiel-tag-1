﻿using ClassLibrary1;
using System;

namespace ConsoleApp6
{
    class Program
    {       
        static void Main(string[] args)
        {
            // Instanz der Klasse
            Class1 test = new Class1();

            int[] intArray = new int[9];

            var ergebnis = test.reader("", "");

            for (int i=0;i<10; i++)
            {
                if(intArray.Length-1 < i)
                {
                    break;
                }
                intArray[i] = i;
                
                Console.WriteLine(i);
            }

            int j = 0;
            while(j < 10)
            {
                Console.WriteLine(j);
                j++;
            }

            do
            {
                Console.WriteLine(j);
                j++;
            }
            while (j < 10);

            if(j<9)
            {

            }
            else if(j<10)
            {

            }
            else
            {
                Console.WriteLine(j);
            }
        }
    }
}
